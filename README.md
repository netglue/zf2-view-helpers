#NetGlue View Helper Bundle

A collection of view helpers mostly for generic websites.

## Installation

Install with composer. Package name on [packagist](https://packagist.org) is `netglue/zf2-view-helpers`
The module name for your ZF2 app config is `NetglueViewHelpers`

## Available Helpers

### HeadIcons

This helper just extends the standard HeadLink view helper with some useful shorcuts and amends the acceptable attributes to include the 'sizes' attribute for Apple touch icons.

####Example in a layout View

```php
	$this->headIcons()->setWindowsIcon($this->basePath('favicon.ico'));
	$this->headIcons()->setShortcutIcon($this->basePath('favicon.png'));
	$this->headIcons()->addAppleTouchIcon($this->basePath('apple-touch-icon-144x144.png'), 144, 'image/png', true);
	$this->headIcons()->addAppleTouchIcon($this->basePath('apple-touch-icon-114x114.png'), 114, 'image/png', true);
	$this->headIcons()->addAppleTouchIcon($this->basePath('apple-touch-icon-72x72.png'), 72, 'image/png', true);
	$this->headIcons()->addAppleTouchIcon($this->basePath('apple-touch-icon.png'), NULL, 'image/png', true);
	echo $this->headIcons()->setSeparator("\n\t");
```

### HeadOg

This helper extends `Zend\View\Helper\HeadMeta` and overrides `isValid()` so that we can output meta properties for pages with an HTML5 doctype. There's a bunch of methods that are helpful in views. It's a bit ugly and doesn't do video and many other useful things but it'll get there...

```php
$this->headOg()->setOgTitle('Set a specific OG Title');
$this->headOg()->setOgDescription('And a specific description');
```

Easier manipulation of the Facebook Admins Property:

```php
$this->headOg()->setFacebookAdmins(array('ID1', 'ID2'));
$this->headOg()->addFacebookAdmin('ID3');
// would render <meta property="fb:admins" content="ID1,ID2,ID3">
```

Convenience setters/getters for `og:type` and `og:site_name`

```php	
$this->headOg()->setOgType('website');
$this->headOg()->setOgSiteName('Foo');
var_dump($this->headOg()->getOgType()); // 'website'
```
	
Add multiple images easily:
```php
// String url required at a bare minimum
$this->headOg()->addImage('http://example.com/image1.png');

// Specify any other valid image property
$this->headOg()->addImage(array(
	'url' => 'http://example.com/image2.png',
	'type' => 'image/png',
	'width' => 100,
	'height' => 100,
	'secure_url' => 'https://example.com/image2.png',
));
```

Applies the head title and meta description to corresponding `og:title` and `og:description` automatically by default to reduce FB Lint errors/warnings

Is constructed by the service manager so you can set options for all pages and selectively add or alter within views only the important stuff

```php
return array(
	'open_graph' => array(
		// Setup any scalar property you like
		'headProperties' => array(
			'fb:page_id' => 'some id',
			'og:site_name' => 'Foo',
			'twitter:card' => 'summary',
			// etc...
		),
		// Arrays of image specs, though as a full uri is required - you would probably add the images from your layout/view
		'images' => array(
			array(
				'url' => 'http://example.com/image2.png',
				'type' => 'image/png',
				'width' => 100,
				'height' => 100,
				'secure_url' => 'https://example.com/image2.png',
			),
		),
		
		// Options:
		'useHeadTitleByDefault' => true, // Get contents of headTitle() helper and bung into a property
		'useMetaDescriptionByDefault' => true, // The same for meta desc
		'addOgUrlByDefault' => true, // Add the full uri of the current request to 'og:url' prop
	),
);
```



## Todo

* As always... Tests...
* OpenGraph Helper - Zend's HeadMeta helper will not render `<meta property>` tags with an HTML5 doctype - whilst they appear to be perfectly valid to me _(W3C Validator allows it anyway)_ It would also be nice to have some concrete methods like addFacebookAdmin() for example

