<?php
/**
 * View Helper Setup
 */

return array(
	'factories' => array(
		'NetglueViewHelpers\View\Helper\HeadOg' => function($sm) {
			$sl = $sm->getServiceLocator();
			$config = $sl->get('Config');
			$helper = new \NetglueViewHelpers\View\Helper\HeadOg;
			if(isset($config['open_graph'])) {
				$options = new \NetglueViewHelpers\View\Helper\HeadOgOptions($config['open_graph']);
				$helper->setOptions($options);
			}
			return $helper;
		},
	),
	'invokables' => array(
		'headIcons' => 'NetglueViewHelpers\View\Helper\HeadIcons',
	),
	'aliases' => array(
		'headOg' => 'NetglueViewHelpers\View\Helper\HeadOg',
	),
);
