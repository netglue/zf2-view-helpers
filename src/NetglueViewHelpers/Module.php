<?php

namespace NetglueViewHelpers;

/**
 * Autoloader
 */
use Zend\Loader\AutoloaderFactory;
use Zend\Loader\StandardAutoloader;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;

/**
 * Service Provider
 */
use Zend\ModuleManager\Feature\ServiceProviderInterface;

/**
 * Config Provider
 */
use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Controller Plugin Provider
 */
//use Zend\ModuleManager\Feature\ControllerPluginProviderInterface;

/**
 * Bootstrap Listener
 */
//use Zend\ModuleManager\Feature\BootstrapListenerInterface;
//use Zend\EventManager\EventInterface as Event;

/**
 * View Helper Provider
 */
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;

class Module implements
	AutoloaderProviderInterface,
	ServiceProviderInterface,
	ConfigProviderInterface,
	//BootstrapListenerInterface,
	//ControllerPluginProviderInterface,
	ViewHelperProviderInterface {
	
	
	/**
	 * Return autoloader configuration
	 * @link http://framework.zend.com/manual/2.0/en/user-guide/modules.html
	 * @return array
	 * @implements AutoloaderProviderInterface
	 */
	public function getAutoloaderConfig() {
    return array(
			AutoloaderFactory::STANDARD_AUTOLOADER => array(
				StandardAutoloader::LOAD_NS => array(
					__NAMESPACE__ => __DIR__,
				),
			),
		);
	}
	
	/**
	 * Include/Return module configuration
	 * @return array
	 * @implements ConfigProviderInterface
	 */
	public function getConfig() {
		return include __DIR__ . '/../../config/module.config.php';
	}
	
	/**
	 * Return Service Config
	 * @return array
	 * @implements ServiceProviderInterface
	 */
	public function getServiceConfig() {
		return include __DIR__ . '/../../config/services.config.php';
	}
	
	/**
	 * Return controller plugin config
	 * @return array
	 * @implements ControllerPluginProviderInterface
	 */
	public function getControllerPluginConfig() {
		return array();
	}
	
	/**
	 * Return view helper plugin config
	 * @return array
	 * @implements ViewHelperProviderInterface
	 */
	public function getViewHelperConfig() {
		return include __DIR__ . '/../../config/view-helpers.config.php';
	}
	
}
