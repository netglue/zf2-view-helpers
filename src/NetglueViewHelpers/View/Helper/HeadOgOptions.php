<?php

namespace NetglueViewHelpers\View\Helper;

use Zend\StdLib\AbstractOptions;

class HeadOgOptions extends AbstractOptions {
	
	protected $useHeadTitleByDefault = true;
	protected $useMetaDescriptionByDefault = true;
	protected $addOgUrlByDefault = true;
	protected $headProperties = array();
	protected $images = array();
	
	
	public function setUseHeadTitleByDefault($flag) {
		$this->useHeadTitleByDefault = (bool) $flag;
		return $this;
	}
	
	public function getUseHeadTitleByDefault() {
		return $this->useHeadTitleByDefault;
	}
	
	public function setUseMetaDescriptionByDefault($flag) {
		$this->useMetaDescriptionByDefault = (bool) $flag;
		return $this;
	}
	
	public function getUseMetaDescriptionByDefault() {
		return $this->useMetaDescriptionByDefault;
	}
	
	public function setAddOgUrlByDefault($flag) {
		$this->addOgUrlByDefault = (bool) $flag;
		return $this;
	}
	
	public function getAddOgUrlByDefault() {
		return $this->addOgUrlByDefault;
	}
	
	public function setHeadProperties(array $properties) {
		$this->headProperties = $properties;
		return $this;
	}
	
	public function getHeadProperties() {
		return $this->headProperties;
	}
	
	public function setImages(array $images) {
		$this->images = $images;
		return $this;
	}
	
	public function getImages() {
		return $this->images;
	}
}