<?php

namespace NetglueViewHelpers\View\Helper;
use stdClass;
use Zend\View\Helper\HeadMeta;
use Zend\View\Exception;
use Zend\View\Helper\Placeholder\Container\AbstractContainer;
use Zend\View\Renderer\RendererInterface as Renderer;


class HeadOg extends HeadMeta {

	protected $options;

	protected $imageProps = array('url', 'secure_url', 'type', 'width', 'height');

	protected $images = array();

	/**
	 * Placeholder Registry Key
	 */
	protected $regKey = 'NetglueViewHelpers_View_Helper_HeadOg';

	/**
	 * Set the open graph type
	 * @param string $type
	 * @return HeadOg
	 */
	public function setOgType($type) {
		return $this->setProperty('og:type', $type);
	}

	/**
	 * Return og:type value
	 * @return string|NULL
	 */
	public function getOgType() {
		return $this->getPropertyValue('og:type');
	}

	/**
	 * Set Open Graph Site Name
	 * @param string $name
	 * @return HeadOg
	 */
	public function setOgSiteName($name) {
		return $this->setProperty('og:site_name', $name);
	}

	/**
	 * Return og:site_name value
	 * @return string|NULL
	 */
	public function getOgSiteName() {
		return $this->getPropertyValue('og:site_name');
	}

	/**
	 * Set facebook admins with an array or string
	 * @param array|string $admins
	 * @return HeadOg
	 */
	public function setFacebookAdmins($admins) {
		if(empty($admins)) {
			return $this->removeFacebookAdmins();
		}
		if(is_array($admins)) {
			$admins = implode(',', $admins);
		}
		return $this->setProperty('fb:admins', $admins);
	}

	/**
	 * Remove the facebook admins property entirely
	 * @return HeadOg
	 */
	public function removeFacebookAdmins() {
		$it = $this->getContainer()->getIterator();
		foreach($it as $key => $item) {
			if(isset($item->property) && $item->property === 'fb:admins') {
				$it->offsetUnset($key);
				break;
			}
		}
		return $this;
	}

	/**
	 * Add a facebook admin
	 * @param string $admin
	 * @return HeadOg
	 */
	public function addFacebookAdmin($admin) {
		if(empty($admin)) {
			return $this;
		}
		$current = $this->getPropertyValue('fb:admins');
		$data = array();
		if(!empty($current)) {
			$data = (strpos($current, ',') !== false) ? explode(',', $current) : array($current);
		}
		$data[] = (string) $admin;
		return $this->setFacebookAdmins($data);
	}

	/**
	 * Set OG Title
	 * @param string $title
	 * @return HeadOg
	 */
	public function setOgTitle($title) {
		return $this->setProperty('og:title', $title);
	}

	/**
	 * Return og:title value
	 * @return string|NULL
	 */
	public function getOgTitle() {
		return $this->getPropertyValue('og:title');
	}

	/**
	 * Set OG Description
	 * @param string $description
	 * @return HeadOg
	 */
	public function setOgDescription($description) {
		return $this->setProperty('og:description', $description);
	}

	/**
	 * Return og:description value
	 * @return string|NULL
	 */
	public function getOgDescription() {
		return $this->getPropertyValue('og:description');
	}

	public function addImage($image) {
		if(is_string($image)) {
			return $this->appendImage($image);
		}
		$args = array('url' => NULL, 'type' => NULL, 'width' => NULL, 'height' => NULL, 'secure_url' => NULL);
		foreach($image as $arg => $val) {
			$arg = strtolower($arg);
			if(array_key_exists($arg, $args)) {
				$args[$arg] = $val;
			}
		}
		return call_user_func_array(array($this, 'appendImage'), $args);
	}

	public function appendImage($url, $type = NULL, $width = NULL, $height = NULL, $secureUrl = NULL) {
		$i = array();
		$i['url'] = $url;
		if(!empty($type)) {
			$i['type'] = $type;
		}
		if(!empty($width)) {
			$i['width'] = $width;
		}
		if(!empty($height)) {
			$i['height'] = $height;
		}
		if(!empty($secureUrl)) {
			$i['secure_url'] = $secureUrl;
		}
		$this->images[] = $i;
		return $this;
	}

	public function getPropertyValue($name) {
		foreach($this->getContainer() as $item) {
			if(isset($item->property) && $item->property === $name) {
				return $item->content;
			}
		}
		return NULL;
	}

	/**
	 * Determine if item is valid
	 *
	 * @param  mixed $item
	 * @return bool
	 */
	protected function isValid($item) {
		if ((!$item instanceof stdClass) || !isset($item->type) || !isset($item->modifiers)) {
			return false;
		}
		if (!isset($item->content) && (! $this->view->plugin('doctype')->isHtml5() || (! $this->view->plugin('doctype')->isHtml5() && $item->type !== 'charset'))) {
			return false;
		}
		// <meta itemprop= ... /> is only supported with doctype html
		if (! $this->view->plugin('doctype')->isHtml5() && $item->type === 'itemprop') {
			return false;
		}
		return true;
	}

	public function getMetaTitle() {
		$view = $this->getView();
		if(!method_exists($view, 'plugin')) {
			return false;
		}
		$ht = $view->plugin('HeadTitle');
		if(is_object($ht)) {
			return $ht->renderTitle();
		}
		return false;
	}

	public function getMetaDescription() {
		$view = $this->getView();
		if(!method_exists($view, 'plugin')) {
			return false;
		}
		$helper = $view->plugin('HeadMeta');
		if(!is_object($helper)) {
			return false;
		}
		foreach($helper->getContainer() as $o) {
			if($o->type === 'name' && $o->name === 'description') {
				return $o->content;
			}
		}
		return false;
	}

	/**
	 * Return the URL for the current request
	 * @return string|false
	 */
	public function getUrl() {
		try {
			$view = $this->getView();
			if(!method_exists($view, 'plugin')) {
				return false;
			}
			$server = $view->plugin('ServerUrl');
			$url = $view->plugin('Url');
			return $server($url());
		} catch(\Exception $e) {
			// Cannot get URL, Likely because there is no route match
			if(isset($_SERVER['REQUEST_URI'])) {
				$path = $_SERVER['REQUEST_URI'];
				if($server) {
					return $server($path);
				}
			}
		}
		return false;
	}

	public function toString($indent = null) {
		$opt = $this->getOptions();
		$title = $this->getOgTitle();
		if(empty($title) && $opt->getUseHeadTitleByDefault()) {
			if($title = $this->getMetaTitle()) {
				$this->setOgTitle($title);
			}
		}
		$desc = $this->getOgDescription();
		if(empty($desc) && $opt->getUseMetaDescriptionByDefault()) {
			if($desc = $this->getMetaDescription()) {
				$this->setOgDescription($desc);
			}
		}
		foreach($this->images as $i) {
			if(!isset($i['url'])) {
			    continue;
			}
			$this->appendProperty('og:image', $i['url']);
			foreach($i as $prop => $val) {
				if($prop !== 'url') {
				    $this->appendProperty('og:image:'.$prop, $val);
				}
			}
		}
		$ogUrl = $this->getPropertyValue('og:url');
		if(empty($ogUrl) && $opt->getAddOgUrlByDefault()) {
			if($ogUrl = $this->getUrl()) {
				$this->setProperty('og:url', $ogUrl);
			}
		}
		return parent::toString($indent);
	}

	public function getOptions() {
		if(!$this->options instanceof HeadOgOptions) {
			$this->options = new HeadOgOptions(array());
		}
		return $this->options;
	}

	public function setOptions(HeadOgOptions $options) {
		$this->options = $options;
		return $this;
	}

	protected function applyOptions() {
		foreach($this->options->getHeadProperties() as $prop => $val) {
			$this->setProperty($prop, $val);
		}
		foreach($this->options->getImages() as $image) {
			$this->addImage($image);
		}
	}

	public function setView(Renderer $view) {
		parent::setView($view);
		$this->applyOptions();
		return $this;
	}

}
