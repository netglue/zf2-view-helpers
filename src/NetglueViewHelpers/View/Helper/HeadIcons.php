<?php

namespace NetglueViewHelpers\View\Helper;
use stdClass;
use Zend\View\Helper\HeadLink;
use Zend\View\Exception;
use Zend\View\Helper\Placeholder\Container\AbstractContainer;

class HeadIcons extends HeadLink {
	
	protected $regKey = 'NetglueViewHelpers_View_Helper_HeadIcons';
	
	/**
	 * Add an apple touch icon
	 * @param string $href
	 * @param string|int|NULL $sizes Either a number or string like '144x144'
	 * @param bool $precomposed
	 * @param string $placement
	 * @return HeadIcons|false
	 * @throws Exception\InvalidArgumentException if $sizes is not a recognisable value for the sizes attribute
	 */
	public function addAppleTouchIcon($href, $sizes, $type = 'image/png', $precomposed = false, $placement = AbstractContainer::APPEND) {
		if(!in_array('sizes', $this->itemKeys)) {
			$this->itemKeys[] = 'sizes';
		}
		$data = array();
		$data['rel'] = ($precomposed) ? 'apple-touch-icon-precomposed' : 'apple-touch-icon';
		$data['href'] = $href;
		$data['type'] = $type;
		
		$size = $this->formatAppleSize($sizes);
		if(false === $size) {
			throw new Exception\InvalidArgumentException("Expected a number or string in the format 144x144 for icon size. Received {$sizes}");
		}
		if(!empty($size)) {
			$data['sizes'] = $size;
		}
		if($icon = $this->getAppleTouchIconAtSize($size)) {
			$icon->rel = $data['rel'];
			$icon->href = $data['href'];
			$icon->type = $data['type'];
			if(!empty($size)) {
				$icon->sizes = $size;
			}
			return $this;
		}
		if(!$this->isDuplicateAppleTouchIcon($data['href'])) {
			return $this->__invoke($data, $placement);
		}
		return false;
	}
	
	/**
	 * Return an appropriate format for an apple touch icon size
	 * @param string|int|NULL $size A number will be formatted as %dx%d. If the format is already correct, nothing is done. If empty, returns NULL and if unregognisable, returns false
	 * @return mixed
	 */
	protected function formatAppleSize($size) {
		if(empty($size)) {
			return NULL;
		}
		if(preg_match('/^[0-9]+x[0-9]+$/', $size)) {
			return $size;
		}
		if(preg_match('/^[0-9]+$/', $size)) {
			return sprintf('%dx%d', $size, $size);
		}
		return false;
	}
	
	/**
	 * Return the apple touch icon at the given size
	 * @param string|int|NULL $size
	 * @return stdClass|false
	 */
	public function getAppleTouchIconAtSize($size) {
		$icon = false;
		$size = $this->formatAppleSize($size = NULL);
		if(false === $size) {
			return false;
		}
		foreach($this->getContainer() as $item) {
			if($item->rel && (strpos($item->rel, 'apple-touch') === 0)) {
				if(empty($size) && !isset($item->sizes)) {
					$icon = $item;
					break;
				}
				if(isset($item->sizes) && ($size === $item->sizes)) {
					$icon = $item;
					break;
				}
			}
		}
		return $icon;
	}
	
	/**
	 * Set the shortcut icon
	 * @param string $href
	 * @param string $type Mime Type
	 * @return HeadIcons
	 */
	public function setShortcutIcon($href, $type = 'image/png') {
		$icon = $this->getOrAppendShortcutIcon();
		$icon->href = $href;
		$icon->type = $type;
		return $this;
	}
	
	/**
	 * Return the unconditional shortcut icon if present in the container
	 * @return stdClass|false
	 */
	public function getShortcutIcon() {
		$icon = false;
		foreach($this->getContainer() as $item) {
			if($item->rel && ($item->rel == 'icon')) {
				$icon = $item;
				break;
			}
		}
		return $icon;
	}
	
	/**
	 * Either return the existing shortcut icon, or create one and append it to the container
	 * @return stdClass
	 */
	protected function getOrAppendShortcutIcon() {
		$icon = $this->getShortcutIcon();
		if(false === $icon) {
			$icon = new stdClass;
			$icon->rel = 'icon';
			$this->append($icon);
		}
		return $icon;
	}
	
	/**
	 * Set the url for the windows favicon.ico with a conditional for IE
	 * @param string $href
	 * @return HeadIcons $this
	 */
	public function setWindowsIcon($href) {
		$icon = $this->getOrAppendWindowsIcon();
		$icon->href = $href;
		return $this;
	}
	
	/**
	 * Return the windows icon if present in the container
	 * @return stdClass|false
	 */
	public function getWindowsIcon() {
		$icon = false;
		foreach($this->getContainer() as $item) {
			if($item->type && ($item->type == 'image/vnd.microsoft.icon')) {
				$icon = $item;
				break;
			}
		}
		return $icon;
	}
	
	/**
	 * Either return the existing windows icon, or create one and append it to the container
	 * @return stdClass
	 */
	protected function getOrAppendWindowsIcon() {
		$icon = $this->getWindowsIcon();
		if(false === $icon) {
			$icon = new stdClass;
			$icon->rel = 'shortcut icon';
			$icon->type = 'image/vnd.microsoft.icon';
			$icon->conditionalStylesheet = 'IE';
			$this->append($icon);
		}
		return $icon;
	}
	
	/**
	 * Whether the given uri is already present in the container as an apple touch icon
	 * @param string $uri
	 * @return bool
	 */
	protected function isDuplicateAppleTouchIcon($uri) {
		foreach ($this->getContainer() as $item) {
			if(($item->rel == 'apple-touch-icon') || ($item->rel == 'apple-touch-icon-precomposed')) {
				if($item->href == $uri) {
					return true;
				}
			}
		}
		return false;
	}
	
}